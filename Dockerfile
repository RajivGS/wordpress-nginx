FROM wordpress:5.7.1-php8.0

# Install necessary PHP extensions
RUN docker-php-ext-install mysqli pdo_mysql

# Set the WordPress configuration
COPY . .
